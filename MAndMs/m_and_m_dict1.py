import random

MAX_VALUE = 100
target = random.randint(1,MAX_VALUE)
mnm_colors_to_values   = {'red':1, 'green':2, 'blue':3, 'yellow':4, 'brown':5, 'orange':6}
mnm_colors_to_quantity = {'red':0, 'green':0, 'blue':0, 'yellow':0, 'brown':0, 'orange':0}

#########################
#  Finding target value #
#########################
print 'Target',target
sum_of_m_n_m = 0

for key in mnm_colors_to_values: # Go through all the keys (in this case it is the colors)
  while(sum_of_m_n_m <= target):
    sum_of_m_n_m += mnm_colors_to_values[key] # Add value of that color
    mnm_colors_to_quantity[key] += 1          # Update that colors quantity
    
  if sum_of_m_n_m > target:                   # If our value is too big, reduce by one m&m
    sum_of_m_n_m -= mnm_colors_to_values[key]
    mnm_colors_to_quantity[key] -= 1

final_sum = 0    
for key in mnm_colors_to_quantity:
  print mnm_colors_to_quantity[key], key, '\tValue: ', mnm_colors_to_values[key]
  final_sum += mnm_colors_to_quantity[key] * mnm_colors_to_values[key]

  
print 'Target:', target, 'Sum of m&m values', final_sum



    
  
