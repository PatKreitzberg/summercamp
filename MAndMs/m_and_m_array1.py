import random

MAX_VALUE = 200
target = random.randint(1,MAX_VALUE)

colors     = ['blue', 'brown', 'yellow', 'blue', 'green', 'red']
values     = [  16,      15,       10,      9,        3,    1  ]
quantities = [   0,       0,        0,      0,        0,    0  ]

#########################
#  Finding target value #
#########################
print 'Target',target
sum_of_m_n_m = 0
for i in range(len(colors)):     # Go through all the colors in order from blue to red
  while(sum_of_m_n_m <= target):
    sum_of_m_n_m  += values[i]   # Add value of the m&m
    quantities[i] += 1           # Update that colors quantity

  if sum_of_m_n_m > target:      # If our value is too big, reduce by one m&m
    sum_of_m_n_m  -= values[i]   
    quantities[i] -= 1

final_sum = 0
for i in range(len(colors)):
  print colors[i], '\t', quantities[i],  '\tValue: ', values[i]
  final_sum += quantities[i] * values[i]
  
print 'Target:', target, 'Sum of m&m values', final_sum
