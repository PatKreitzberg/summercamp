import random

MAX_VALUE = 200
target = random.randint(1,MAX_VALUE)

colors     = ['blue', 'brown', 'yellow', 'blue', 'green', 'red']
values     = [  16,      15,       10,      9,        3,    1  ]
quantities = [   0,       0,        0,      0,        0,    0  ]

#########################
#  Finding target value #
#########################
print 'Target',target
sum_of_m_n_m = 0
for i in range(len(colors)):     # Go through all the colors in order from blue to red
  while(sum_of_m_n_m <= target):
    sum_of_m_n_m  += values[i]   # Add value of the m&m
    quantities[i] += 1           # Update that colors quantity

  if sum_of_m_n_m > target:      # If our value is too big, reduce by one m&m
    sum_of_m_n_m  -= values[i]   
    quantities[i] -= 1

final_sum = 0
for i in range(len(colors)):
  print colors[i], '\t', quantities[i],  '\tValue: ', values[i]
  final_sum += quantities[i] * values[i]
  
print 'Target:', target, 'Sum of m&m values', final_sum
print

######################################
#  Finding All Possible Combinations #
######################################
max_num_blue = 1+target/values[colors.index('blue')]
for num_blue in range(max_num_blue + 1):
  max_num_brown = 1 + target/values[colors.index('brown')]
  for num_brown in range(max_num_brown + 1):
    max_num_yellow = 1 + target/values[colors.index('yellow')]
    for num_yellow in range(max_num_yellow + 1):
      max_num_orange = 1 + target/values[colors.index('orange')]
      for num_orange in range(max_num_orange + 1):
        max_num_green = 1 + target/values[colors.index('green')]
        for num_green in range(max_num_green + 1):
          max_num_red = 1 + target/values[colors.index('red')]
          for num_red in range(max_num_red + 1):
            if num_blue*values[colors.index('blue')] + num_brown*values[colors.index('brown')] + num_yellow*values[colors.index('yellow')] + num_orange*values[colors.index('orange')] + num_green*values[colors.index('green')] + num_red*values[colors.index('red')] == target:
              print 'Blue', num_blue, '\tBrown', num_brown, '\tYellow', num_yellow, '\tOrange', num_orange, '\tGreen', num_green, '\tRed', num_red
