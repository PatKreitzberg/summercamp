# python3

def print_board(board):
  for i in range(len(board)):
    row = board[i]
    print(row[0], '|', row[1], '|', row[2])
    if i != len(board)-1:
      print('---------')

def row_winner(row):
  if row[0] == 'X' and row[1] == 'X' and row[2] == 'X':
    return True
  if row[0] == 'O' and row[1] == 'O' and row[2] == 'O':
    return True
  return False

def winner(board):
  # check rows for winners:
  for row in board:
    if row_winner(row):
      return True
  # check columns for winners:
  for i in range(3):
    column = [ board[j][i] for j in range(3) ]
    if row_winner(column):
      return True  
  # check diagonals for winners:
  first_diag = [ board[0][0], board[1][1], board[2][2] ]
  if row_winner(first_diag):
    return True
  second_diag = [ board[0][2], board[1][1], board[2][0] ]
  if row_winner(second_diag):
    return True
  # no winner!
  return False

def draw(board):
  for row in board:
    for cell in row:
      if cell == ' ':
        return False
  return True

def make_move(board, player):
  print('Make your move,', player, '!')
  invalid_move = True
  while invalid_move:
    # get input from player!
    move = input()
    print('Move was', move)
    row_move, col_move = move.split()
    row_move = int(row_move)
    col_move = int(col_move)
    if board[row_move][col_move] != ' ':
      print('Nice try, bucko!')
    else:
      invalid_move = False
  board[row_move][col_move] = player

def next_player(player):
  # change to other player
  if player == 'X':
    return 'O'
  else:
    return 'X'

board = [[" ", " ", " "], [" ", " ", " "], [" ", " ", " "]]
player = 'X'

while not winner(board) and not draw(board):
  print('Current board is:')
  print_board(board)
  make_move(board, player)
  player = next_player(player)

if winner(board):
  print('You won!')

if draw(board):
  print('Too bad, no winner')
