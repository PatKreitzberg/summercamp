import random
import numpy

# Example program for factoring a product of two primes (i.e.,
# breaking RSA encryption) using brute force

target = 11282281

print "Target: ", target

# Find all factors:
found_factor = False
for x in xrange(2,1+target/2):
  for y in xrange(1,x+1):
    if x*y == target: #x,y are factors of target
      found_factor = True
      print 'Pair of factors: ', x, y

if found_factor is False:
  print "No factors found, ", target, "must be prime!"
