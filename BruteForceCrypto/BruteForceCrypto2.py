import random

# Example program which finds all prime divisors of a target value

MAX_VALUE = 10000
target = random.randint(10,MAX_VALUE)

def find_divisors(z):
  # This function finds a pair of divisors for z
  for x in range(1+z/2):
    for y in range(x+1):
      if x*y == z: # x,y are divisors of z
        return x,y
  return None,None # No divisors found, return None for x, y

def find_all_divisors(target):
  prime_divisors = []
  divisors_stack = []
  divisors_stack.append(target)
  # divisors_stack holds the numbers we want to factor.
  # Everytime find_divisors() returns two numbers they can not be
  # prime so we append them to divisors_stack
  while(len(divisors_stack) > 0):
    z = divisors_stack.pop() # Remove element from divisors_stack while we find its divisors
    x,y = find_divisors(z)
    if x is None or y is None:
      prime_divisors.append(z)
    else:
      divisors_stack.append(x)
      divisors_stack.append(y)
  return prime_divisors

prime_divisors = find_all_divisors(target)
print 'Target: ', target
print 'Prime divisors: ', prime_divisors
