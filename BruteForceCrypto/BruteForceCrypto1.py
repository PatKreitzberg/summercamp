import random
import time

# Example program to find pairs of divisors of a randomly generated target number

MAX_VALUE = 10000
target = random.randint(10,MAX_VALUE) # Get a random target value

print "Target: ", target

# Find all factors:
found_factor = False
for x in range(2,1+target/2):
  for y in range(1,x+1):
    if x*y == target: #x,y are factors of target
      found_factor = True
      print 'Pair of factors: ', x, y

if found_factor is False:
  print "No factors found, ", target, "must be prime!"



