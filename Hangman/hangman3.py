import random
import os
import time


##############################
#       Read in file         #
##############################

# Array to hold the list of words
words = []

f = open('word-list.txt')    # Open the text file
for line in f:               # Iterate of each line in the file
  words.append(line.strip())
  # strip() function removes the newline character '\n' at the end of each line

f.close()


################################
# Pick a random word from list #
################################
word_index = random.randint(0,len(words)) # Random number
target_word = words[word_index] # Word located at random number


# Initialize variables
revealing_word = ['*']*len(target_word) # Make array of * with same length as the target word.
success_or_failure = False # If this is True the game ends
guesses = ''
misses  = 0
MAX_MISSES = 7

################################
#  Loop through user guesses   #
################################
def get_guess():
  # Repeat input if guess has already beeen guessed
  # or if guess is not just one character.
  guess = raw_input("Your guess: ")
  while(guess in guesses or len(guess) > 1 or len(guess) is 0):
    guess = raw_input("Guess again: ")
  return guess


def find_guess_in_target(target_word, revealing_word, guess):
  # See if the users guess is in the target word, if so:
  #   Update revealing_word to show correctly guess letter
  #   Return correct_guess boolean
  correct_guess = False
  for i in range(len(target_word)): # Look at every character in the target word
    if target_word[i] is guess:
      revealing_word[i] = guess[0]
      correct_guess = True
  return revealing_word, correct_guess

# Main loop
while(success_or_failure is False):
  print 'Hangman:   ', revealing_word
  print 'Guesses:   ', guesses

  guess = get_guess()
  guesses  += guess
  revealing_word, correct_guess = find_guess_in_target(target_word, revealing_word, guess)

  if correct_guess is False:
    misses += 1
    if misses >= MAX_MISSES:
      print 'Sorry, you lose!'
      print 'The word was', target_word
      success_or_failure = True
    
  # If there are no more *s in the array then every letter has been guessed correctly
  if '*' not in revealing_word:
    success_or_failure = True
    print 'Hangman:   ', revealing_word
    print "Congratulations! You got the word \"" + target_word + "\""

  print # Prints blank line for aesthetics
