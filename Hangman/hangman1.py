import random
import os
import time


##############################
#       Read in file         #
##############################

words = [] # Array to hold the words from the file

f = open('word-list.txt')    # Open the text file
for line in f:               # Iterate through each line in the file
  words.append(line.strip())
  # strip() function removes the newline, '\n', at the end of each line

f.close()
