import random
import os
import time


##############################
#       Read in file         #
##############################

words = [] # Array to hold the words from the file

f = open('word-list.txt')    # Open the text file
for line in f:               # Iterate through each line in the file
  words.append(line.strip())
  # strip() function removes the newline, '\n', at the end of each line

f.close()


####################################
# Print words which start with 'h' #
####################################

for word in words:
  if word[0] is 'h':
    print word

####################################
#   Print words with 'h' and 'l    #
####################################
    
for word in words:
  word_has_an_h = False
  word_has_an_l = False
  
  for letter in word:
    if letter is 'h':
      word_has_an_h = True
    if letter is 'l':
      word_has_an_l = True

  if word_has_an_l is True and word_has_an_h is True:
    print word
